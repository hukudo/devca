#!/bin/bash
set -euo pipefail


CFSSL_URL=https://github.com/cloudflare/cfssl/releases/download/v1.6.0/cfssl_1.6.0_linux_amd64
JQ_URL=https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
DEVCA_URL=https://gitlab.com/hukudo/devca/-/raw/master/bin/devca-create

set -x
sudo wget -q $CFSSL_URL -O /usr/local/bin/cfssl
sudo chmod +x /usr/local/bin/cfssl
sudo wget -q $JQ_URL -O /usr/local/bin/jq
sudo chmod +x /usr/local/bin/jq
sudo wget -q $DEVCA_URL -O /usr/local/bin/devca-create
sudo chmod +x /usr/local/bin/devca-create
