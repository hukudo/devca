Your own certificate authority (CA) for development.
It generates wildcard certificates.


# Installation
Check bash version, install [cfssl][] and devca-create.
```
/usr/bin/env bash --version  # tested with bash 4.4
wget -q https://gitlab.com/hukudo/devca/-/raw/master/install.sh -O - | sudo bash
```

[cfssl]: https://github.com/cloudflare/cfssl


# Trust your CA
For Debian / Ubuntu:
```
sudo install --mode 644 ~/.devca/CA.crt /usr/local/share/ca-certificates/devca.crt
sudo update-ca-certificates  # this creates a symlink in /etc/ssl/certs/devca.pem
```

For other distributions and browsers please see [trust.md](trust.md).


# Try the Nginx example
```
sudo -i bash <<'EOF'
echo "127.0.0.1 foo.localhost" >> /etc/hosts
EOF

cd examples/nginx/
git clean -fx

devca-create foo.localhost etc_nginx/certs/
docker-compose up -d

curl -I https://foo.localhost

docker-compose down
```
