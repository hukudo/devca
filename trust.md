# Trusting a Certificate Authority (CA)
Given the file `~/.devca/CA.crt` file, here is how to trust it.

## Linux
### Debian / Ubuntu
```
sudo install --mode 644 ~/.devca/CA.crt /usr/local/share/ca-certificates/devca.crt
sudo update-ca-certificates  # this creates a symlink in /etc/ssl/certs/devca.pem
```

### Arch Linux
```
sudo trust anchor --store ~/.devca/CA.crt
```

## Firefox
Here is how to add a certificate authority manually:

- open Preferences
- search "cert"
- click "View Certificates"
- in the "Authorities" tab
  - click Import and select `~/.devca/CA.crt`
  - set "Trust this CA to identify websites"

![Firefox: How to add a Certificate Authority][firefox-vid]

[firefox-vid]: https://gitlab.com/hukudo/devenv/devcerts/-/wikis/uploads/5d46e33ed7a3e7ae9b800f93091e6151/firefox--how-to-add-a-certificate-authority.webm

## Mozilla NSS
Mozilla's [Network Security Services][nss] is used in [many open source
projects][nss-overview].
```
hash certutil || sudo apt-get install libnss3-tools
certutil -A -n devca -t "C,p,p" -i ~/.devca/CA.crt -d sql:$HOME/.pki/nssdb
```

[nss]: https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS
[nss-overview]: https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/Overview

## Chrome
- Settings > Manage Certificates > Authorities: Import
  - `~/.devca/CA.crt`
  - "Trust this certificate for identifying websites"


# Removing Trust
```
certutil -d sql:$HOME/.pki/nssdb -D -n devca

sudo rm /usr/local/share/ca-certificates/devca.crt
sudo update-ca-certificates

curl https://foop.localhost
# SSL certificate problem: unable to get local issuer certificate
```

- open Preferences
- search "cert"
- click "View Certificates"
- in the "Authorities" tab
  - select "0-main devcerts Root CA"
  - click "Delete or Distrust"
  - confirm


