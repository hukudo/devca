#!/usr/bin/env python
# encoding: utf-8
import logging
import os
import re
import ssl
import sys

from http.server import HTTPServer, SimpleHTTPRequestHandler
from pathlib import Path

logging.basicConfig(level=os.environ.get('LOGLEVEL', 'INFO').upper())
log = logging.getLogger('server')

here_relative = Path(__file__).parent
here = here_relative.absolute()

usage = f"""\
{sys.argv[0]} CERT_DIR SERVER_SPEC

Starts a simple http server like "python -mhttp.server", but with TLS enabled.

The CERT_DIR must contain two files: `crt` and `key`.

For example

    {sys.argv[0]} certs/python3.localhost 0.0.0.0:8443

will be available at

    https://python3.localhost:8443
"""

try:
    cert_dir = Path(sys.argv[1])
    server_spec = sys.argv[2]

    RE_SERVER_SPEC = re.compile(r'^([^:]+):(\d+)$')

    m = RE_SERVER_SPEC.match(server_spec)
    hostname = m.group(1)
    port = int(m.group(2))
except IndexError:
    print(usage)
    raise SystemExit(1)

domain_name = cert_dir.name
keyfile = cert_dir / 'key'
certfile = cert_dir / 'crt'

log.info(f'keyfile={keyfile}')
log.info(f'certfile={certfile}')
log.info(f'starting to listen on {hostname}:{port}')
log.info(f'URL: https://{domain_name}:{port}')

httpd = HTTPServer((hostname, port), SimpleHTTPRequestHandler)

httpd.socket = ssl.wrap_socket(
    httpd.socket,
    keyfile=str(keyfile),
    certfile=str(certfile),
    server_side=True
)

httpd.serve_forever()
