#!/bin/bash
set -euo pipefail

here="$(readlink -m $(dirname $0))"
cd $here


set -x
docker run \
  --name devca-examples-nginx \
  -p 80:80 \
  -p 443:443 \
  -v ./etc_nginx/:/etc/nginx:ro \
  -v ./favicon.ico:/var/www/favicon.ico:ro \
  devca-examples-nginx
